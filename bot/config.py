from os import environ

from ruamel.yaml import safe_load


class Config:
    def __init__(self):
        self.config = safe_load(open("config.yml"))

        self.token = self.config.get("token") or environ.get("TOKEN") or ""
        self.owner_id = self.config.get("token") or environ.get("OWNER_ID") or ""
        self.log_channel = self.config.get("token") or environ.get("LOG_CHANNEL") or ""
